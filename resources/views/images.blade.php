<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
    
    <style>
        .filterDiv {
            display: none;
        }
        .show {
            display: block;
        }
        .btn {
            border: none;
            outline: none;
            padding: 12px 16px;
            background-color: #f1f1f1;
            cursor: pointer;
        }
        .btn:hover {
            background-color: #ddd;
        }
        .btn.active {
            background-color: #666;
            color: white;
        }
    </style>
    <title>Gambar-Gambar Mangga</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <ul class="navbar-nav">
            <li class="navbar-brand">Azure Mangga</li>
            <li class="nav-item">
            <a class="nav-link" href="/">Mangga</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="create">Upload</a>
            </li>
        </ul>
    </nav>
    <div class="container p-5">
        <h1>Semua Gambar Mangga</h1>
        <div id="myBtnContainer">
            <button class="btn active" onclick="filterSelection('all')"> Show all</button>
            <button class="btn" onclick="filterSelection('Madu')"> Madu</button>
            <button class="btn" onclick="filterSelection('Manalagi')"> Manalagi</button>
            <button class="btn" onclick="filterSelection('Arumanis')"> Arumanis</button>
            <button class="btn" onclick="filterSelection('Golek')"> Golek</button>
        </div>
        <table id="mytable" class="table table-bordered">
            <tr>
                <th>Jenis Mangga</th>
                <th>Gambar</th>
                <th>Deskripsi</th>
                <th>Donwload</th>
            </tr>
            <?php 
                    function test($item) {
                        return strpos($item, '.png');
                    }
                    $path = Storage::url('images/rxggoBceDIouZhkGjPNKdeQ2IFzYw3j9zx57E1vh.png');
                    $x = array_filter(Storage::disk('azure')->files("uploads"));
                    
                    
            ?>
            @for ($i = 0; $i < count($x); $i++)
            <?php    
                $temp = explode("_",$x[$i]);
                // $imgname = explode(".",$temp[1])[0];
                $t = explode("/",$temp[0])[1];
            ?>
            {{-- $url = Storage::disk('azure')->url($x[$i]); --}}
            <tr>
                <th>
                    <?php echo $t ;?>
                </th>
                <th><img width="100px" src="{{ url(Storage::disk('azure')->url($x[$i])) }}" alt="" srcset=""></th>
                <th>Deskripsi Mangga</th>
                <th><a href="{{ url(Storage::disk('azure')->url($x[$i])) }}">Donwload Now</a></th>
                
                <!-- 
                {{-- @dump($img) --}}
                {{-- <th><img width="100px" src="{{ url($test) }}" alt="" srcset=""></th> --}}
                {{-- <th><a href="{{ url($test) }}">Donwload Now</a></th> --}} -->
            </tr>
            @endfor
        </table>
        <script>
            // console.log(document.getElementsByClassName("filterDiv"));
            filterSelection("all")
            function filterSelection(c) {
                if (c == "all") c = "";
                var table, tr, td, txtValue,i;
                table = document.getElementById("mytable");
                tr = table.getElementsByTagName("tr");
                // tr2 = tr.getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                    th = tr[i].getElementsByTagName("th")[0];
                    if (th) {
                        txtValue = th.textContent || th.innerText;
                        if (txtValue.indexOf(c) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }       
                }
            }

            var btnContainer = document.getElementById("myBtnContainer");
            var btns = btnContainer.getElementsByClassName("btn");
            for (var i = 0; i < btns.length; i++) {
                btns[i].addEventListener("click", function(){
                    var current = document.getElementsByClassName("active");
                    current[0].className = current[0].className.replace(" active", "");
                    this.className += " active";
                });
            }
        </script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </div>
  </body>
</html>