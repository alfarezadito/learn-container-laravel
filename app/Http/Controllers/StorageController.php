<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class StorageController extends Controller
{
    //
    public function getData(Request $request)
    {
        $path = $request->file('avatar')->store('avatars');

        return $path;
    }

    public function putData(Request $request)
    {
        $content = "resources\assets\testupload.jpg";
        Storage::put('avatars/1', $content);

        return $path;
    }
}
