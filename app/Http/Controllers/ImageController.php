<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
 
class ImageController extends Controller
{
    public function index()
    {
        return view("images");
    }
    public function create()
    {
        return view('create');
    }
 
    public function store(Request $req)
    {
        $this->validate($req,[
            'file' => 'required|file|max:7000'
            ]);
    
        $t = $req -> jenis;
        if($req->file()) 
        {
            $fileName = $t.'_'.$req->file->getClientOriginalName();
            // save file to azure blob virtual directory uplaods in your container
            $filePath = $req->file('file')->storeAs('uploads/', $fileName, 'azure');
    
            return redirect()
            ->back()
            ->with('success','File has been uploaded.');
    
        }


        // $this->validate($req, [
        //     'featured_image' => 'required|file|max:7000', // max 7MB
        // ]);

        // $path = Storage::putFile(
        //     'public/images',
        //     $req->file('featured_image'),
        // );
 
        // return redirect()
        //     ->back()
        //     ->withSuccess("Image succes Uploaded in " . $path);
    }

    public function getallimage(Request $req)
    {
        array_filter(Storage::disk('azure')->files(),
        //only png's
        function ($item) {return strpos($item, '.png');}
        );
        return array_filter;
    }
}
