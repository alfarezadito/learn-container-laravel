<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('images');
});
// Route::get('/set', function () {
//     return 'taek';
// });

// Route::post("store", "StorageController@putData")->name("putdata");

Route::get("create", "App\Http\Controllers\ImageController@create");
Route::post("store", "App\Http\Controllers\ImageController@store")->name("store");
Route::post("home", "App\Http\Controllers\ImageController@getallimage");
Route::get("images", "App\Http\Controllers\ImageController@index");